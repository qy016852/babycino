class TestFeatureB {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {

    public int f(){
	boolean a;
	int x;
	a = false;
	x = 5;
	
	// comparing integer with boolean
	if (a != x) {
		System.out.println(x);
	} else {
		System.out.println(0);
	}
	return x;
    }

}

